# Orange Pi Zero 2 W MiSTeX Buildroot

Run [build.sh](build.sh) or select buildroot path to run:

```bash
cd <YOUR_BUILDROOT_ROOT_FOLDER>
make BR2_EXTERNAL=<THIS_REPOSITORY_ROOT> mistex_defconfig
# This builds the sdcard image and the SDK
make BR2_EXTERNAL=<THIS_REPOSITORY_ROOT> sdk
```

Image successfully built in [Arch Linux](https://archlinux.org/) with these packages:

```
sudo pacman -Syyu
sudo pacman -S base-devel file wget unzip git make cmake bc rsync cpio dosfstools mtools
```

## Build Main MiSTeX binary

You will need to define where is buildroot folder into `BR2_ROOT` environment variable before compiling:

```
git submodule update --init --recursive
cd Main_MiSTeX
git apply ../patches/Main_MiSTeX_01.patch
make BR2_ROOT=<YOUR_BUILDROOT_ROOT_FOLDER>
```

## Prepare your Media folder

Copy the MiSTer binary and its files file to `media` folder and make the image

```
cd ../media
cp Main_MiSTeX/MiSTer .
git clone https://github.com/MiSTeX-devel/Distribution_MiSTeX.git
mv Distribution_MiSTeX/* .
rm -rf Distribution_MiSTeX
rm -rf .git
```

## Flash the Media Files into the FAT partition

```bash
cd <YOUR_BUILDROOT_ROOT_FOLDER>
make BR2_EXTERNAL=<THIS_REPOSITORY_ROOT> mistex_defconfig
# Rebuild your image with the 
make BR2_EXTERNAL=<THIS_REPOSITORY_ROOT>
```
## Work in Progress to Mainline Linux SPI Support for OPi02w

I have been a bit busy these last days but I have got some work in progress:

The first thing I want to say is the botched attempts to get a working spidev in the device tree:

- Tried to use Mainline Linux Kernel 6.1, 6.7 and 6.8

- Used the .dtb files from Official Orange Pi Zero 2W Debian Image to replace the compiled blob (of Mainline Linux Kernel 6.7) in /boot folder

- Used a .dts for orangepi-zero2w found in https://kernel.googlesource.com/pub/scm/linux/kernel/git/next/linux-next-history/+/master/arch/arm64/boot/dts/allwinner/sun50i-h618-orangepi-zero2w.dts

- As in the `external/packages/pack-uboot/sun50iw9/bin/dts/orangepizero2w-u-boot-current.dts` and `external/packages/pack-uboot/sun50iw9/bin/dts/orangepizero3-u-boot-current.dts` sources in the [Official Vendor Image Build Environment](https://github.com/orangepi-xunlong/orangepi-build) the `.dts` are disabled. I have patched .dts (3 different ways) to activate spi1/spidev in device tree, I don't have much experience on that but the patch I made which made more sense to me was [0001-enable-spi1-spidev-linux.patch](board/orangepi/orangepi-zero2w/patches/0001-enable-spi1-spidev-linux.patch), but it didn't work... (**TO DO: test patching the u-boot .dts**)

- I have found [another patch](https://patchwork.ozlabs.org/project/uboot/patch/20240130141642.2714470-1-andre.przywara@arm.com/), but I don't remember that worked for SPI, I believe not.

#### Things to highlight:

- Mainline kernel linux 6.7 is very reliable, it was the best experience on stability I have ever faced in Orange Pi Zero 2W (specially when handling USB devices)

- I was wondering about using the Orange Pi fork 6.1-sun50iw9 kernel with their .dtb for buildroot, but I am following your orientations to keep effort in using a good mainline kernel.

- `U-boot-2024.01`, `linux-headers-6.7` and `linux-6.7` using the original `orangepizero3.dts`, which are placed in:

```shell
./linux-headers-6.7/arch/arm64/boot/dts/allwinner/sun50i-h618-orangepi-zero3.dts
./linux-6.7/arch/arm64/boot/dts/allwinner/sun50i-h618-orangepi-zero3.dts
./uboot-2024.01/arch/arm/dts/sun50i-h618-orangepi-zero3.dts
```

- `spidev` is built and linked into the kernel as a module:

```
(...)
HDRINST usr/include/linux/spi/spidev.h
(...)
CC [M]  drivers/spi/spidev.o
(...)
CC [M]  drivers/spi/spidev.mod.o
(...)
 LD [M]  drivers/spi/spidev.ko
(...)
  INSTALL /home/chandler/Documentos/buildroot/output/target/lib/modules/6.7.0/kernel/drivers/spi/spidev.ko
  STRIP   /home/chandler/Documentos/buildroot/output/target/lib/modules/6.7.0/kernel/drivers/spi/spidev.ko
(...)
```

- In the target:

```
# find /lib/modules/6.7.0 -name spi
/lib/modules/6.7.0/kernel/drivers/spi
/lib/modules/6.7.0/kernel/drivers/net/can/spi
# ls /lib/modules/6.7.0/kernel/drivers/spi
spi-bcm2835.ko        spi-meson-spifc.ko    spi-rzv2m-csi.ko
spi-bcm2835aux.ko     spi-mtk-nor.ko        spi-sh-msiof.ko
spi-dw-mmio.ko        spi-omap2-mcspi.ko    spi-tegra114.ko
spi-dw.ko             spi-qcom-qspi.ko      spi-tegra210-quad.ko
spi-geni-qcom.ko      spi-rockchip-sfc.ko   spidev.ko
spi-imx.ko            spi-rpc-if.ko
spi-meson-spicc.ko    spi-rspi.ko
```

- In the host side, you have:

```
$ find output/build/linux-6.7 -name "*spidev*"
output/build/linux-6.7/Documentation/spi/spidev.rst
output/build/linux-6.7/drivers/spi/spidev.c
output/build/linux-6.7/drivers/spi/spidev.o
output/build/linux-6.7/drivers/spi/.spidev.o.cmd
output/build/linux-6.7/drivers/spi/spidev.mod
output/build/linux-6.7/drivers/spi/.spidev.mod.cmd
output/build/linux-6.7/drivers/spi/spidev.mod.c
output/build/linux-6.7/drivers/spi/spidev.mod.o
output/build/linux-6.7/drivers/spi/.spidev.mod.o.cmd
output/build/linux-6.7/drivers/spi/spidev.ko
output/build/linux-6.7/drivers/spi/.spidev.ko.cmd
output/build/linux-6.7/include/uapi/linux/spi/spidev.h
output/build/linux-6.7/tools/spi/spidev_fdx.c
output/build/linux-6.7/tools/spi/spidev_test.c
```

- I have not found any difference in `drivers/spi/spidev.c` from mainline linux kernel 6.7 and `orange-pi-6.1-sun50iw9` from the [Official Linux Vendor Kernel](https://github.com/orangepi-xunlong/linux-orangepi) at branch `orange-pi-6.1-sun50iw9`, both are placed in the kernel:

```
# ls /lib/modules/$(uname -r)/kernel/drivers/spi/spidev.ko
/lib/modules/6.7.0/kernel/drivers/spi/spidev.ko
# lsmod | grep spidev
spidev                 16384   0
```

- For MiSTeX `/dev/i2c-0` must be in device tree, too. But it is actually listed in mainline kernel.

- There are no printk statements on both sources, when kernel is running there are no dmesg logs about SPI after boot process.

- There is actually a service in `/etc/init.d/S_00SPI` which starts SPI kernel module and stops it in poweroff

- The only SPI service log in dmesg is in boot, which is:

![](spi_failure.png)

- In `external/packages/pack-uboot/sun50iw9/bin/dts/orangepizero2w-u-boot-current.dts` source in the [Official Vendor Image Build Environment](https://github.com/orangepi-xunlong/orangepi-build) the addresses for `SPI0` (QSPI flash) and `SPI1` (GPIO pins) are:

```text
spi0 = "/soc@3000000/spi@5010000";
spi1 = "/soc@3000000/spi@5011000";
```
